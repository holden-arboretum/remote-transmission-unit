# Remote Transmission Unit

An arduino sketch written to run on the Feather32u4 lora enabled board

## Dependencies

- Adafruit_Sensor
- OneWire
- DHT22
- Arduino Lmic
- CayenneLPP
- DHT Sensor library
- LMIC
- soil-moisture (written by me)
- ATBirchy85 (written by me)

## Notes

- This code will take readings from all the sensors and transmit them every 10 seconds
- It may take a while  to join the lora network but be patient. If it does not happen, then there is most likely an issue with the gateway.  
- There are a few very important lines in this RTU firmware that must be changed with each individual RTU device it is uploaded to. They are unique identifiers
  which allow the TTN application I have created to distinguish each RTU from one another. Instructions to perform this task are as follows:  
  - When a new RTU needs to be registered to TTN website, sign in to TTN and enter the console. In the applications page, click the only application listed: "Holden Arboretum RTU Data Decoder"  
  - You will then be able to register a new device in that application in the devices section. Choose a name that will distinguish it from the others and enter it into the Device ID field  
  - Next to the Device EUI field, click the arrows button to have the field automatically generated.  
  - The App Key field should also be set to be automatically generated.  
  - Hit the register button  
  - The new device will be registered and the Device EUI and the App Key will be assigned  
  - You will need to copy the generated values and replace the old values in the RTU firmware:  
  - There is a certain syntax that these strings have to be pasted into the code, fortunately it is easy to         manipulate the given values  
  - The Device EUI needs to be saved into the  
```cpp
static const u1_t PROGMEM DEVEUI[8] 
```
variable in least significant bit notation. If you press the <> button next to the field, it will change the    notation to C-style and by pressing the arrows button immediately to the right, it will change the notation     between msb and lsb. Set it to lsb, copy the string, and replace the existing string in the firmware with       this new one  
  - Unlike the Device EUI string, the App Key string needs to be in the most significant bit notation in the        firmware. Change its field to C-style and it should change and set itself in the msb notation. Copy this        string and replace the previous string for the 
 ```cpp
 static const u1_t PROGMEM APPKEY[16]  
 ```
  variable.
  - The Application EUI should not be changed, as it is universal between all RTUs


- We decided to upgrade the antenna on the RTU to try and increase its maximum range. The instructions and parts to perform this upgrade can be found [here](https://learn.adafruit.com/adafruit-feather-32u4-radio-with-lora-radio-module/antenna-options)

- This code fills up 99% of the memory of the RTU. This may be an issue going forward if additional code needs to be added for power consumption purposes

-Frequently, when the RTU is powered on and starts transmitting readings to the gateway, it takes up to 4 or 5 transmissions before the gateway successfully uploads data readings to TTN. After these initial missed transmissions, the gateway consistently receives the transmissions being sent. In order to compensate for this, a counter is utilized to ensure that a transmission is sent whenever the device is powered on.

```cpp
os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(SEND_INTERVAL), do_send);
  if (x == 4)
  {
      pinMode(11, OUTPUT);
      digitalWrite(11, HIGH);
      break;
  }
  else
  {
      break;
  }
  ```
 
The 4 value can be changed to allow for more or less transmissions to be sent before the device powers off.
 
