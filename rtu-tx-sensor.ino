#include <CayenneLPP.h>
#include <OneWire.h>
#include <DHT.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <ATBirchy85.h>
#include <VH400.h>

//Set up Soil Temp Sensor
int DS18S20_Pin = A2;
OneWire ds(DS18S20_Pin);

// Set up Temp/Hum sensor
#define DHTTYPE DHT22
#define DHTPIN A1

#define VH400_PIN A3

DHT dht(DHTPIN, DHTTYPE);
VH400 vh400(VH400_PIN);
ATBirchy85 birchy(23);

// Define our device LoRaWAN parameters
static const u1_t PROGMEM DEVEUI[8]= { 0x0F, 0xE3, 0x9F, 0x8A, 0x22, 0xCD, 0xBF, 0x00 };
static const u1_t PROGMEM APPEUI[8]= { 0x3B, 0xBF, 0x02, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };
static const u1_t PROGMEM APPKEY[16] = { 0x5E, 0x44, 0x5E, 0x58, 0x93, 0x37, 0xC8, 0xE0, 0x9B, 0x6F, 0x8C, 0xA0, 0x13, 0xA8, 0xD8, 0x5E };

// helper methods
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

int x = 0;

// Initalize our CayenneLPP Encoder
CayenneLPP lpp(51);

static osjob_t sendjob, blinkjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned SEND_INTERVAL = 10;

const lmic_pinmap lmic_pins = {
   .nss = 8,
   .rxtx = LMIC_UNUSED_PIN,
   .rst = 4,
   .dio = {7,9,LMIC_UNUSED_PIN}
};

void onEvent (ev_t ev) {
    switch(ev) {
        case EV_TXCOMPLETE:
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(SEND_INTERVAL), do_send);
            
            if (x == 4)
            {
            pinMode(11, OUTPUT);
            digitalWrite(11, HIGH);
            break;
            }
            else
            {
            break;
            }
            
            
    }
}

void do_send(osjob_t* j){
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        // do nothing
    } else {
        // Prepare upstream data transmission at the next possible time.
        lpp.reset();

        // Do sensor reads here
        float temp = dht.readTemperature(true);
        float hum = dht.readHumidity();
        float light = birchy.get_light();
        digitalWrite(A0, HIGH);
        delay(1000); //Need a delay to allow the voltage to stabilize before taking a reading from the soil moisture sensor
        float soil_m = vh400.read();
        digitalWrite(A0, LOW);
        float soil_t = getTemp();

        // Encode the raw data
        lpp.addTemperature(1, temp);
        lpp.addRelativeHumidity(2, hum);
        lpp.addAnalogInput(3, soil_m);
        lpp.addTemperature(4, soil_t);
        lpp.addAnalogInput(5, light);

        LMIC_setTxData2(1, lpp.getBuffer(), lpp.getSize(), 0);
    }
}

void setup() {
    // Initialize the temp/hum sensor
    dht.begin();
    pinMode(13, OUTPUT);
    pinMode(A2, INPUT);
    pinMode(11, INPUT);
    

    // LMIC init
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    LMIC_reset();
    LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);

    // Start job (sending automatically starts OTAA too)
    do_send(&sendjob);
    
}


//Method to return the temperature from one DS18S20 in DEG Celsius
//This replaces the DallasTemperature library because it takes up too much space in memory
float getTemp(){
  

  byte data[12];
  byte addr[8];

  ds.search(addr);
  ds.reset();
  ds.select(addr);
  ds.write(0x44,1); // start conversion, with parasite power on at the end

  byte present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE); // Read Scratchpad

  
  for (int i = 0; i < 9; i++) { // we need 9 bytes
    data[i] = ds.read();
  }
  
  ds.reset_search();
  
  byte MSB = data[1];
  byte LSB = data[0];

  float tempRead = ((MSB << 8) | LSB); //using two's compliment
  float TemperatureSum = tempRead / 16;
  return TemperatureSum;
  
}

void loop() {
    os_runloop_once();
}
